# README #

* This project focuses on creating a generic framework for representing a medical pipeline and executing it across a distributed cluster for a large number of patients. Two example pipelines were considered: knee cartilage MRI analysis and DNA sequencing.

* We have designed a robust JSON specification for representing a pipeline. The data dependency information contained within this spec is used to represent the pipeline as a directed acyclic graph (DAG). The structure of this graph is used to prioritize different stages of the pipeline. To effectively distribute work across a cluster, we created a master & worker paradigm using Python and MPI.  This system supports dynamic work scheduling using the aforementioned DAG.  We implemented a simple filesystem based checkpointing system to limit the amount of wasted CPU time due to a system failure.  We have completed unit testing with over 99% code coverage. We also have a thorough black box testing suite.


### Project Webpage ###
http://www4.ncsu.edu/~abahman/Pipeline.html

### How do I get set up? ###

* Please refer to docs/installation_manual.pdf


### Contributors (alphabetically ordered) ###
* Amir Bahmani
* Sarah Baucom
* Monis Khan
* Hussein Koprly
* Spencer Moore
* Frank Mueller
* Marc Niethammer
* Kouros Owzar
* Mahmoud Parsian
* Alexander Sibley
* Martin Styner

### Thanks To ###
Lisandro D. Dalcin for supporting [mpi4py](http://pythonhosted.org/mpi4py/)!

### Who do I talk to? ###
* Amir Bahmani <abahman@ncsu.edu>
* Frank Mueller <mueller@cs.ncsu.edu>